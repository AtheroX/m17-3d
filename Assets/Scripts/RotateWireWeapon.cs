﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWireWeapon : MonoBehaviour{

	private Vector3 startpos;
	public GameEvent onGetWeapon;

	void Start() {
		startpos = transform.position;	
	}

	/// <summary>
	/// Rotates the weapon on its pedestal
	/// </summary>
	void Update() {
		transform.Rotate(new Vector3(-.5f, -1.25f, 0));
		transform.position = startpos + new Vector3(0,Mathf.Sin(Time.time)/3f,0);

		if(Physics.OverlapSphere(startpos, 1).Length > 0) {
			onGetWeapon.Raise();
			Destroy(gameObject);
		}
    }
}
