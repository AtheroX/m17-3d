﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Moves this object to player position
/// It's a target for the camera
/// </summary>
public class CameraController : MonoBehaviour{
	
	public Transform player;


	void Update() {
		transform.position = player.transform.position;
	}
}
