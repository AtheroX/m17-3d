﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Event that is called when triggers its collider
/// </summary>
[RequireComponent(typeof(Collider))]
public class OnTriggerEvent : MonoBehaviour{

	public GameEvent startRace;

	public void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Player") 
			startRace.Raise();	
	}
}
